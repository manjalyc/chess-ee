import java.util.ArrayList;


public class ChessBoard {
    private char[][] myBoard = {
            {'r','n','b','q','k','b','n','r'},
            {'p','p','p','p','p','p','p','p'},
            {' ',' ',' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' ',' ',' '},
            {'P','P','P','P','P','P','P','P'},
            {'R','N','B','Q','K','B','N','R'},
    };

    public ChessBoard(){}


    public static String toPos(int row, int column){
        return "" + (char)(97+column) + (char)(56 - row);
    }
    public static int getRowFromRank(char rank){
        return 56 - (int)rank;
    }
    public static int getRowFromPos(String pos){
        return 56 - (int)pos.charAt(1);
    }
    public static int getColFromFile(char file){
        return (int)file - 97;
    }
    public static int getColFromPos(String pos){
        return (int)pos.charAt(0)-97;
    }


    public char getPiece(String pos){
        return myBoard[getRowFromPos(pos)][getColFromPos(pos)];
    }
    public char getPiece(int row, int col){
        return myBoard[row][col];
    }

    public void setPiece(String pos, char piece){
        myBoard[getRowFromPos(pos)][getColFromPos(pos)] = piece;
    }

    public void removePiece(String pos){
        myBoard[getRowFromPos(pos)][getColFromPos(pos)] = ' ';
    }

    public void movePiece(String source, String dest){
        if(source == null || dest == null) throw new IllegalArgumentException("source: " + source + ",dest: " + dest);
        Tracker.logTaken(getPiece(source), getPiece(dest));
        removePiece(dest);
        setPiece(dest, getPiece(source));
        setPiece(source, ' ');
    }

    public void printBoard(){
        System.out.println("     a   b   c   d   e   f   g   h\n   +---+---+---+---+---+---+---+---+");
        for(int i = 0; i < 8; i++) {
            System.out.print(" " + (8-i) + " |");
            for(int j = 0; j < 8; j++){
                System.out.print(" " + myBoard[i][j] + " |");
            }
            System.out.println(" " + (8-i) + "\n   +---+---+---+---+---+---+---+---+");
        }
        System.out.println("     a   b   c   d   e   f   g   h");
    }

    public String getPositionOf(char piece){
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if( myBoard[i][j] == piece) return toPos(i,j);
                else if (myBoard[i][j] == piece) return toPos(i,j);
            }
        }
        return null;
    }
    public ArrayList<String> getAllPositionsOf(char piece){
        ArrayList<String> positions = new ArrayList<>();
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if( myBoard[i][j] == piece) positions.add(toPos(i,j));
                else if (myBoard[i][j] == piece) positions.add(toPos(i,j));
            }
        }
        return positions;
    }
    public ArrayList<String> getAllAltPositionsOf(char piece){
        ArrayList<String> positions = new ArrayList<>();
        for(int i = 7; i >= 0; i--){
            for(int j = 7; j >= 0; j--){
                if( myBoard[i][j] == piece) positions.add(toPos(i,j));
                else if (myBoard[i][j] == piece) positions.add(toPos(i,j));
            }
        }
        return positions;
    }

    public String rowScan(String pos, char piece) {
        int row = getRowFromPos(pos);
        int col = getColFromPos(pos);
        for (int j = col+1; j < 8; j++) {
            if (myBoard[row][j] == piece) return toPos(row, j);
            else if (myBoard[row][j] != ' ') break;
        }
        for (int j = col-1; j >= 0; j--) {
            if (myBoard[row][j] == piece) return toPos(row, j);
            else if (myBoard[row][j] != ' ') break;
        }
        //System.out.println("rowScan failed: " + pos + ":" + piece);
        //printBoard();
        return null;
    }
    public String colScan(String pos, char piece) {
        int row = getRowFromPos(pos);
        int col = getColFromPos(pos);
        for (int i = row+1; i < 8; i++) {
            if (myBoard[i][col] == piece) return toPos(i, col);
            else if (myBoard[i][col] != ' ') break;
        }
        for (int i = row-1; i >= 0; i--) {
            if (myBoard[i][col] == piece) return toPos(i, col);
            else if (myBoard[i][col] != ' ') break;
        }
        //System.out.println("colScan failed: " + pos + ":" + piece);
        //printBoard();
        return null;
    }
    public String diagonalScan(String pos, char piece) {
        int row = getRowFromPos(pos);
        int col = getColFromPos(pos);
        //to upper left
        for (int d = 1; row-d >= 0 && col + d < 8; d++) {
            if (myBoard[row-d][col + d] == piece) return toPos(row-d, col + d);
            else if (myBoard[row-d][col + d] != ' ') break;
        }
        //to upper right
        for (int d = 1; row + d < 8 && col + d < 8; d++){
            if (myBoard[row+d][col + d] == piece) return toPos(row+d, col + d);
            else if (myBoard[row+d][col + d] != ' ') break;
        }
        //to lower right
        for (int d = 1; row + d < 8 && col - d >= 0; d++){
            if (myBoard[row+d][col - d] == piece) return toPos(row+d, col - d);
            else if (myBoard[row+d][col - d] != ' ') break;
        }
        //to lower left
        for (int d = 1; row-d >= 0 && col - d >= 0; d++){
            if (myBoard[row-d][col - d] == piece) return toPos(row-d, col - d);
            else if (myBoard[row-d][col - d] != ' ') break;
        }
        //System.out.println("diagonalScan failed: " + pos + ":" + piece);
        //printBoard();
        return null;
    }

    public String findSourcePawnInCol(String dest, boolean white){
        int row = getRowFromPos(dest);
        int col = getColFromPos(dest);
        if(white){
            if(myBoard[row+1][col] == 'P') return toPos(row+1,col);
            else if(myBoard[row+2][col] == 'P') return toPos(row+2,col);
        }
        else{
            if(myBoard[row-1][col] == 'p') return toPos(row-1,col);
            else if(myBoard[row-2][col] == 'p') return toPos(row-2,col);
        }
        return null;
    }
    public String findSourcePawnInCross(String dest, char sc, boolean white){
        int row = getRowFromPos(dest);
        int col = getColFromFile(sc);
        if(white){
            if(myBoard[row+1][col] == 'P') return toPos(row+1,col);
        }
        else{
            if(myBoard[row-1][col] == 'p') return toPos(row-1,col);
        }
        return null;
    }
    public String findSourceBishop(String dest, boolean white){
        if(white) return diagonalScan(dest, 'B');
        return diagonalScan(dest, 'b');
    }
    public String findSourceRook(String dest, boolean white){
        String ret = null;
        if(white){
            ret = rowScan(dest, 'R');
            if(ret == null) ret = colScan(dest, 'R');
        }
        else{
            ret = rowScan(dest, 'r');
            if(ret == null) ret = colScan(dest, 'r');
        }
        return ret;
    }
    public String findSourceKnight(String dest, boolean white){
        int row = getRowFromPos(dest);
        int col = getColFromPos(dest);
        if(white){
            for(String knightPos : getAllPositionsOf('N')){
                int sr = getRowFromPos(knightPos);
                int cr = getColFromPos(knightPos);
                if( row-sr != 0 && col - cr != 0 && Math.abs(row-sr) + Math.abs(col-cr) == 3) return knightPos;
            }
        }
        else{
            for(String knightPos : getAllPositionsOf('n')){
                int sr = getRowFromPos(knightPos);
                int cr = getColFromPos(knightPos);
                if( row-sr != 0 && col - cr != 0 && Math.abs(row-sr) + Math.abs(col-cr) == 3) return knightPos;
            }

        }
        return null;
    }
    public String findSourceKnightAlt(String dest, boolean white){
        int row = getRowFromPos(dest);
        int col = getColFromPos(dest);
        if(white){
            for(String knightPos : getAllAltPositionsOf('N')){
                int sr = getRowFromPos(knightPos);
                int cr = getColFromPos(knightPos);
                if( row-sr != 0 && col - cr != 0 && Math.abs(row-sr) + Math.abs(col-cr) == 3) return knightPos;
            }
        }
        else{
            for(String knightPos : getAllAltPositionsOf('n')){
                int sr = getRowFromPos(knightPos);
                int cr = getColFromPos(knightPos);
                if( row-sr != 0 && col - cr != 0 && Math.abs(row-sr) + Math.abs(col-cr) == 3) return knightPos;
            }

        }
        return null;
    }
    public String findSourceKing(boolean white){
        if(white) return getPositionOf('K');
        return getPositionOf('k');
    }
    public String findSourceQueen(String dest, boolean white){
        if(white) return getPositionOf('Q');
        return getPositionOf('q');
    }

    public String toString(){
        String ret = "";
        for(int i = 0; i < 8; i++){
            for(int j=0; j <8; j++){
                ret += myBoard[i][j];
            }
            ret += "|";
        }
        return ret;
    }

    public void fromString(String board){
        String[] rowArray = board.split("\\|");
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                myBoard[i][j] = rowArray[i].charAt(j);
            }
        }
    }
}
