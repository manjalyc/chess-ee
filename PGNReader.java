import java.io.BufferedReader;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.io.FileReader;

public class PGNReader {
    public static void main(String[] args)throws Throwable{
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        String[] sfiles = {"KingBase2018-A00-A39.pgn",
                "KingBase2018-C00-C19.pgn",
                "KingBase2018-E00-E19.pgn",
                "KingBase2018-A40-A79.pgn",
                "KingBase2018-C20-C59.pgn",
                "KingBase2018-E20-E59.pgn",
                "KingBase2018-A80-A99.pgn",
                "KingBase2018-C60-C99.pgn",
                "KingBase2018-E60-E99.pgn",
                "KingBase2018-B00-B19.pgn",
                "KingBase2018-D00-D29.pgn",
                "KingBase2018-B20-B49.pgn",
                "KingBase2018-D30-D69.pgn",
                "KingBase2018-B50-B99.pgn",
                "KingBase2018-D70-D99.pgn"};
        List<String> files = Arrays.asList(sfiles);
        Collections.sort(files);
        for(String file : files) {
            int c = 0;
            int b = 0;
            BufferedReader inFile = new BufferedReader(new FileReader("data/" + file));
            System.out.println("Processing " + file + "...");
            String x = " ";
            while (!inFile.ready()) System.out.println(c);
            while (x != null) {
                String game = inFile.readLine();
                if (game.indexOf("1.") == 0) {
                    c++;
                    String r = inFile.readLine();
                    while (r != null && !r.contains("[")) {
                        game += "\n" + r;
                        r = inFile.readLine();
                    }
                    x = r;
                    try {
                        Game mg = new Game(game);
                        while (!mg.gameIsOver) mg.nextMove();
                    }
                    catch (Exception e) {
                        System.out.println("\tUnable to Process Game: #" + c + " | " + e);
                        b++;
                    }

                }
            }
            System.out.println("\tFailed to process " + b + "/" + c + " games in " + file);
        }

        Tracker.printMatrix();
        Tracker.printSystems();
    }
}
