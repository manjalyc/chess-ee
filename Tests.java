public class Tests {

    public static void quickDebugView(String hist){
        Game.debugOutput = true;
        quickView(hist);
        Game.debugOutput = false;
    }

    public static void quickView(String hist){
        Game n = new Game(hist);
        while(!n.gameIsOver) n.nextMove();
        System.out.println(n.getHistory());
        System.out.println(n.toString());
        n.printBoard();
    }

    public static void main(String[] args){
        ChessBoard x = new ChessBoard();
        x.printBoard();
        char[] tr= {'r','n','b','q','k','b','n','r'};
        char[] br= {'R','N','B','Q','K','B','N','R'};
        boolean passed = true;
        for(int i = 0; i < 8; i++){
            /**
            System.out.println(x.getPiece((char)(97+i) + "7")  == 'p');
            System.out.println(x.getPiece((char)(97+i) + "2")  == 'P');
            System.out.println(x.getPiece((char)(97+i) + "1")  == br[i]);
            System.out.println(x.getPiece((char)(97+i) + "8")  == tr[i]);
             **/
            if(
                    !((x.getPiece((char)(97+i) + "7")  == 'p') &&
                    (x.getPiece((char)(97+i) + "2")  == 'P') &&
                    (x.getPiece((char)(97+i) + "1")  == br[i]) &&
                    (x.getPiece((char)(97+i) + "8")  == tr[i]))
            )System.out.println("Initial Mapping Failed");
        }

        Game y = new Game("");
        y.board.movePiece("b7","b5");
        System.out.println(!y.canPieceMoveDiagonallyTo("b5","e5"));
        System.out.println(y.canPieceMoveDiagonallyTo("b5","e2"));
        System.out.println(y.canPieceMoveDiagonallyTo("b5","d7"));
        System.out.println(y.canPieceMoveDiagonallyTo("b5","a6"));
        System.out.println(y.canPieceMoveDiagonallyTo("b5","a4"));
        System.out.println(y.canPieceMoveDiagonallyTo("d5","a8"));
        y.board.movePiece("c2","c4");
        //y.printBoard();

        //todo FIX THIS SHIT
        System.out.println(y.canPieceMoveDiagonallyTo("d1","a4"));
        String g1 = "1.Nf3 Nf6 2.g3 e6 3.Bg2 d5 4.O-O Be7 5.c4 dxc4 6.Qa4+ c6 7.Qxc4 b5 8.Qc2 \n" +
                "Bb7 9.d4 O-O 10.Rd1 Nbd7 11.Ne5 Nxe5 12.dxe5 Nd7 13.Nc3 a6 14.Ne4 Qc7 15.\n" +
                "Bf4 Nb6 16.Nd6 Bxd6 17.exd6 Qd7 18.e4 f6 19.Be3 c5 20.Bxc5 Rac8 21.Rac1 \n" +
                "Nc4 22.b4 f5 23.Qe2 Rce8 24.Rxc4 bxc4 25.Qxc4 fxe4 26.Bxe4 Bxe4 27.Qxe4 \n" +
                "Rf5 28.Re1 Rd5 29.h4 a5 30.a3 axb4 31.axb4 Qf7 32.Kg2 Qd7 33.Re3 Rf5 34.\n" +
                "Ra3 Rd5 35.Ra7 Qb5 36.Qg4 g6 37.Qf3 Rf5 38.Qb7 Qxb7+ 39.Rxb7 Rf7 40.Rxf7 \n" +
                "Kxf7 41.Bb6 1-0";
        String g1v = "    r   |     k p| B Pp p |        | P     P|      P |     PK |        |";
        String g2 = "1.Nf3 Nf6 2.c4 e6 3.g3 d5 4.Bg2 dxc4 5.Qa4+ Bd7 6.Qxc4 c5 7.O-O Bc6 8.Nc3 \n" +
                "Nbd7 9.d3 Be7 10.Qb3 O-O 11.Bf4 Nd5 12.Nxd5 Bxd5 13.Qc2 Rc8 14.b3 b6 15.\n" +
                "Rac1 Bxf3 16.Bxf3 Bg5 17.Bxg5 Qxg5 18.Bg2 Rfd8 19.a4 Qe7 20.a5 b5 21.Qb2 \n" +
                "Nf6 22.Rc2 Nd5 23.Rfc1 Nb4 24.Rc3 h5 25.h4 g6 26.Qd2 Rc7 27.Qf4 Na2 28.b4 \n" +
                "e5 29.Qg5 Nxc3 30.Rxc3 cxb4 31.Qxe7 Rxe7 32.Rb3 a6 33.Rxb4 Rc8 34.Bd5 Rc1+\n" +
                "35.Kg2 Ra1 36.f4 exf4 37.Rxf4 Kg7 38.Kf2 Rxa5 39.Bc6 Ra2 40.Bf3 a5 41.d4 \n" +
                "b4 42.d5 b3 43.d6 Rd7 44.Rd4 b2 45.Be4 Ra1 0-1";
        String g2v = "        |   r pk |   P  p |p      p|   RB  P|      P | p  PK  |r       |";
        String g3 = "1.f4 Nh6 2.Nf3 g6 3.e3 Bg7 4.c4 c6 5.Nc3 Nf5 6.Be2 d5 7.d4 O-O 8.O-O e6 9.\n" +
                "Qe1 Nd7 10.g4 Nd6 11.c5 Ne4 12.Nxe4 dxe4 13.Ne5 f6 14.Nc4 e5 15.Nd6 exd4 \n" +
                "16.exd4 Qe7 17.Bc4+ Kh8 18.Qxe4 Qxe4 19.Nxe4 f5 20.gxf5 Bxd4+ 21.Kh1 Rxf5 \n" +
                "22.Nd6 Nxc5 23.Nxf5 Bxf5 24.h4 Rd8 25.a4 Bf6 26.Ra3 Ne4 27.Kh2 Nd2 28.Bxd2\n" +
                "Rxd2+ 29.Kg3 Rxb2 30.Rb3 Rxb3+ 31.Bxb3 b5 32.Rc1 Be4 33.axb5 cxb5 34.Rc8+ \n" +
                "Kg7 35.Rc7+ Kh6 36.Bg8 Bg7 37.Rxa7 b4 38.Re7 Bc6 39.Rc7 Be4 40.Rc4 Bf5 41.\n" +
                "Rxb4 Bc3 42.Rb1 Bxb1 43.Be6 Kg7 44.h5 Be1+ 45.Kg4 Kf6 46.Bg8 Bf5+ 47.Kf3 \n" +
                "gxh5 48.Kg2 h4 49.Kf3 Bg3 50.Bd5 h5 51.Bc6 Bg4+ 52.Kg2 Bxf4 53.Be8 Kg5 54.\n" +
                "Bc6 h3+ 55.Kg1 Bg3 56.Ba8 Be6 57.Bb7 Bf5 58.Ba8 Kf4 59.Bb7 Be1 60.Kh1 Kg3 \n" +
                "61.Ba8 Bf2 62.Bb7 Bd3 63.Ba8 Bf1 0-1";
        String g3v="B       |        |        |       p|        |      kp|     b  |     b K|";
        String g4 = "1.c4 e5 2.g3 c6 3.Nf3 e4 4.Nd4 d5 5.cxd5 Qxd5 6.Nc2 Nf6 7.Nc3 Qh5 8.Ne3 \n" +
                "Bh3 9.Qc2 Bxf1 10.Rxf1 Qe5 11.f3 exf3 12.Rxf3 Na6 13.Nf5 O-O-O 14.d4 Qe6 \n" +
                "15.Bg5 h6 16.Re3 Nb4 17.Qb1 Qc4 18.Bxf6 gxf6 19.a3 Nd5 20.Nxd5 Qxd5 21.Qe4\n" +
                "Qa5+ 22.Kf1 Bb4 23.Rc1 Rd5 24.Rf3 Bd6 25.b4 1-0";
        String g4v = "  k    r|pp   p  |  pb p p|q  r N  | P PQ   |P    RP |    P  P|  R  K  |";
        String g5 = "1. e4 e5 2. Nf3 Nf6 3. c3 a5 4. Bc4 d5 5. g3 d4 6. b4 dxc3 7. b5 Be7 8. d3 c5 9. bxc6 1-0";
        String g5v = "rnbqk  r| p  bppp|  P  n  |p   p   |  B P   |  pP NP |P    P P|RNBQK  R|";
        String g6 = " 1.Nf3 Nf6 2.g3 d5 3.Bg2 c6 4.O-O Bg4 5.b3 e6 6.Bb2 Nbd7 7.d3 Be7 8.Nbd2 O-O 9.h3 Bh5 10.g4 Bg6 11.Nh4 e5 12.e3 Ne8 13.Nxg6 hxg6 14.Qe2 Nc7 15.c4 Re8 16.Rac1 d4 17.Ne4 Nc5 18.exd4 exd4 19.Qd2 a5 20.Rce1 N5e6 21.a3 g5 22.Ng3 Nf4 23.Ne2 Nce6 24.Nxf4 Nxf4 25.Re4 Bf6 26.Rfe1 Rxe4 27.Rxe4 g6 28.Bf1 Qb6 29.h4 Qxb3 30.Bxd4 Bxd4 31.Rxd4 Ne6 32.Rd7 gxh4 33.d4 Qxa3 34.d5 Nc5 35.dxc6 Nxd7 36.cxb7 Rd8 37.Qxd7 Rxd7 38.b8=Q Kh7 39.Qb5 Rd4 40.Qg5 Qf3 0-1";
        String g6v = "        |     p k|      p |p     Q |  Pr  Pp|     q  |     P  |     BK |";

        //quickDebugView(g5);


        Game mg = new Game(g1);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g1v));
        mg = new Game(g2);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g2v));
        mg = new Game(g3);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g3v));
        mg = new Game(g4);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g4v));
        mg = new Game(g5);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g5v));
        mg = new Game(g6);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g6v));

        String g7 = " 1.Nf3 Nf6 2.g3 d5 3.Bg2 c6 4.d3 Bf5 5.Nbd2 e6 6.O-O h6 7.Qe1 Be7 8.e4 Bh7 9.Qe2 Na6 10.c3 Nc5 11.Ne5 d4 12.Nb3 Nxb3 13.axb3 O-O 14.Ra4 Bc5 15.Nf3 b5 16.Ra6 dxc3 17.bxc3 Bb6 18.Be3 Nd7 19.Rfa1 c5 20.Bf4 Re8 21.d4 b4 22.cxb4 cxd4 23.e5 Rc8 24.h4 Rc2 25.Qb5 d3 26.Nd2 g5 27.hxg5 hxg5 28.Rxb6 axb6 29.Be3 Qc7 30.Nc4 Rc8 31.Nd6 d2 32.Bxd2 Rxd2 33.Nxc8 Qc3 34.Ra8 Qd4 35.Ne7 Kg7 36.Qc6 Nxe5 37.Qb7 Qxf2 38.Kh1 Rd1 39.Kh2 Qg1 40.Kh3 g4 41.Kh4 Qh2 42.Kg5 Qh6# 0-1";
        String g7v = "R       | Q  Npkb| p  p  q|    n K | P    p | P    P |      B |   r    |";
        mg = new Game(g7);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g7v));

        String g8 = " 1.Nf3 d5 2.c4 e6 3.g3 dxc4 4.Bg2 Nf6 5.O-O c6 6.a4 a5 7.Qc2 Na6 8.Ne5 Nb4 9.Qxc4 g6 10.d4 Bg7 11.Bd2 Nfd5 12.e4 Nb6 13.Qc5 Bf8 14.Qc1 Bg7 15.Nf3 O-O 16.Rd1 Qe7 17.Be3 Nd7 18.Na3 b6 19.Nc4 Ba6 20.Bf4 Bxc4 21.Qxc4 e5 22.Bg5 Bf6 23.Bh6 Rfd8 24.d5 Qc5 25.Qe2 cxd5 26.exd5 Rac8 27.Nd2 Qc2 28.Qf3 Bg7 29.Bxg7 Kxg7 30.Ne4 f5 31.Ng5 Nf8 32.Qe3 Qxb2 33.Qxb6 Nd3 34.Qe3 e4 35.Rab1 Qf6 36.Bxe4 Nc5 37.Bg2 h6 38.Nf3 Nxa4 39.Rb7 Rd7 40.Rxd7 Nxd7 41.Qa7 Nc5 42.Qxa5 f4 43.Nd4 Ne5 44.Nc6 Ncd3 45.Qa7 Kh8 46.Be4 Nb2 47.Rb1 Nec4 48.Ne7 Re8 49.d6 Qxd6 50.Nxg6 Kg8 51.Nxf4 1-0";
        String g8v = "    r k |Q       |   q   p|        |  n BN  |      P | n   P P| R    K |";
        mg = new Game(g8);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g8v));


        String g9 = " 1.c4 e6 2.g3 d5 3.Bg2 c6 4.Qc2 Bd6 5.Nf3 Ne7 6.d4 O-O 7.O-O f5 8.b3 Nd7 9.Ba3 Bxa3 10.Nxa3 Ng6 11.Qd2 Qe7 12.Nc2 f4 13.e3 fxg3 14.fxg3 dxc4 15.bxc4 e5 16.Qc3 Nb6 17.Nd2 Bf5 18.e4 exd4 19.Nxd4 Bg4 20.c5 Nd7 21.Qb3 Kh8 22.Qxb7 Qxc5 23.Nb3 Qc3 24.Qxc6 Qe3 25.Kh1 Nge5 26.Qd5 Rae8 27.Nf5 Bxf5 28.exf5 Nf6 29.Qd4 Qh6 30.Qf4 Qh5 31.h3 Qf7 32.g4 Nd3 33.Qf3 Ne5 34.Qg3 Nc4 35.Rfe1 Qd7 36.Qf4 Nb2 37.Nc5 Qb5 38.Ne6 Nd3 39.Qf1 a6 40.Red1 Nxg4 41.hxg4 Rxe6 42.Qxd3 Re1 43.Kh2 Qxd3 44.Rxd3 Rxa1 45.Ra3 Rf6 46.Bb7 Rb6 47.Bxa6 Rb2 48.Kg3 Rbxa2 49.Re3 Ra3 50.Kf4 Rxe3 0-1";
        String g9v = "       k|      pp|B       |     P  |     KP |    r   |        |r       |";
        mg = new Game(g9);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g9v));

        String g10 = " 1.Nf3 Nf6 2.c4 b6 3.g3 Bb7 4.Bg2 g6 5.b3 Bg7 6.Bb2 O-O 7.O-O c5 8.d4 cxd4 9.Nxd4 Bxg2 10.Kxg2 d5 11.cxd5 Qxd5 12.Nf3 Qb7 13.Nd2 Nc6 14.a3 Rfd8 15.Rc1 Rac8 16.Rc2 e5 17.Qa1 Ne8 18.Rfc1 f6 19.b4 Ne7 20.Rxc8 Rxc8 21.Qa2 Kf8 22.Rxc8 Qxc8 23.Qc4 Nd6 24.Qxc8 Nexc8 25.e4 Ke7 26.Kf1 Ke6 27.Ke2 Ne7 28.Kd3 Bh6 29.Nc4 Nxe4 30.Kxe4 f5 31.Kd3 e4 32.Ke2 exf3 33.Kxf3 Kd5 34.Ne3 Bxe3 35.Kxe3 Kc4 36.Kf4 Kb3 37.Bc1 Kc2 38.Be3 Nd5 39.Ke5 Nxe3 40.fxe3 Kd3 41.Kf4 b5 42.h4 h6 43.h5 gxh5 44.Kf3 a6 45.Kf4 Ke2 46.Kxf5 Kxe3 47.Kg6 Kf3 48.Kxh5 Kxg3 49.Kxh6 Kf4 50.Kg6 Ke5 51.Kf7 Kd5 52.Ke7 Kc4 53.Kd6 Kb3 0-1";
        String g10v = "        |        |p  K    | p      | P      |Pk      |        |        |";
        mg = new Game(g10);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g10v));

        String g11 = " 1.Nf3 c5 2.g3 Nc6 3.Bg2 g6 4.e4 Bg7 5.O-O e5 6.d3 Nge7 7.c3 O-O 8.Nh4 d5 9.Nd2 Be6 10.f4 exf4 11.gxf4 dxe4 12.dxe4 f5 13.exf5 gxf5 14.Qe2 Bd5 15.Ndf3 Qd6 16.Be3 b6 17.Rfd1 Qf6 18.Ng5 Rad8 19.Bxd5 Nxd5 20.Qf3 Nde7 21.Rxd8 Rxd8 22.Rd1 Rxd1 23.Qxd1 h6 24.Qb3 Kh8 25.Ngf3 Na5 26.Qb5 Qc6 27.Qd3 Qe6 28.b4 Nc4 29.Bf2 cxb4 30.cxb4 Qe4 31.Qd1 Kh7 32.Qd7 Bf6 33.Nd4 Ne3 34.Ndf3 Qxf4 35.h3 Nd5 36.Qxa7 Qxb4 37.Qd7 Qb1 0-1";
        String g11v = "        |   Qn  k| p   b p|   n p  |       N|     N P|P    B  | q    K |";
        mg = new Game(g11);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g11v));

        String g12 = " 1.Nf3 c5 2.c4 Nf6 3.g3 b6 4.Bg2 Bb7 5.O-O e6 6.Nc3 Be7 7.Re1 d6 8.e4 a6 9.d4 cxd4 10.Nxd4 Qc7 11.Qe2 Nbd7 12.Bd2 O-O 13.Rac1 Rac8 14.b3 Rfe8 15.g4 Nf8 16.g5 N6d7 17.f4 Qb8 18.f5 Ne5 19.Bh3 Bd8 20.Rf1 g6 21.fxe6 fxe6 22.Rxf8 Rxf8 23.Bxe6 Kg7 24.Bxc8 Qxc8 25.Rf1 Rxf1 26.Qxf1 b5 27.cxb5 Bb6 28.Be3 Qg4 29.Qg2 Nf3 30.Nxf3 Bxe3 31.Kf1 Qc8 32.Qc2 Qh3 33.Qg2 Qc8 34.Qc2 Qh3 1/2-1/2";
        String g12v = "        | b    kp|p  p  p | P    P |    P   | PN bN q|P Q    P|     K  |";
        mg = new Game(g12);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g12v));

        String g13 = " 1.b3 Nf6 2.Bb2 d5 3.e3 g6 4.Nf3 Bg7 5.d4 O-O 6.Bd3 Nbd7 7.O-O b6 8.c4 Bb7 9.cxd5 Nxd5 10.Nc3 Nxc3 11.Bxc3 c5 12.Rc1 Rc8 13.d5 Bxc3 14.Rxc3 Bxd5 15.Ba6 Bxf3 16.Qxf3 Rc7 17.Rd1 Ne5 18.Qe2 Rd7 19.Rxd7 Qxd7 20.f4 Ng4 21.Rd3 Qf5 22.Bb7 Nf6 23.Bf3 h5 24.g3 Qc8 25.e4 Rd8 26.Re3 e6 27.e5 Nd5 28.Rd3 Ne7 29.Be4 Rd4 30.Kf2 Qd7 31.Qd2 Kg7 32.Ke3 Nd5 33.Kf3 Nc7 34.Ke3 Nb5 35.a4 Rxd3 36.Qxd3 Nd4 37.Qc4 Nc6 38.Qd3 Nd4 39.Qc4 Qc8 40.b4 Qc7 41.a5 Qe7 42.bxc5 bxc5 43.Qa6 Qc7 44.Kd3 Nf5 45.Qb7 Qxa5 46.Bxf5 gxf5 47.Qe7 Qa3 48.Ke2 Qa2 49.Kf1 Qd5 50.Qxa7 c4 51.Qc7 Qd3 52.Kg2 c3 53.Kh3 c2 54.Qe7 Qd1 55.Qf6 Kf8 56.Qh8 Ke7 57.Qf6 Ke8 58.Qh8 Kd7 59.Qf8 c1=Q 60.Qxf7 Kd8 61.Qf8 Kc7 62.Qe7 Qd7 63.Qb4 Qf1 64.Kh4 Qfd1 65.Qa5 Kb7 66.Qb4 Kc8 67.Qc5 Qc7 68.Qf8 Qcd8 69.Qxd8 Kxd8 70.h3 Ke7 71.Kg5 Qf3 72.Kh4 Kf7 73.Kg5 Qxg3 74.Kxh5 Kg7 75.h4 Qg6 0-1";
        String g13v = "        |      k |    p q |    Pp K|     P P|        |        |        |";
        //quickDebugView(g13);
        mg = new Game(g13);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g13v));

        String g14 = " 1.c4 g6 2.Nc3 Bg7 3.g3 c5 4.Bg2 Nc6 5.Nf3 d6 6.O-O e6 7.d3 Nge7 8.Bg5 h6 9.Bd2 b6 10.Rb1 Bb7 11.a3 Qd7 12.b4 Rd8 13.e3 O-O 14.Qe2 Ba8 15.Rfd1 Qc8 16.Nb5 g5 17.h3 Ng6 18.Bc3 e5 19.Be1 f5 20.Nc3 Nce7 21.Nh2 Bxg2 22.Kxg2 Rf7 23.a4 Rdf8 24.a5 cxb4 25.Rxb4 Nc6 26.Rb5 Nxa5 27.Nd5 Nc6 28.Qh5 Nge7 29.Ra1 e4 30.d4 f4 31.Qg4 Rf5 32.Nxe7 Nxe7 33.Rxa7 fxe3 34.Rxe7 exf2 35.Rxf5 fxe1=Q 36.Rxf8 Qxf8 37.Qe6 Kh7 38.Rf7 Qd2 39.Kh1 Qe1 40.Kg2 Qe2 41.Kh1 Qd1 42.Kg2 Qc2 43.Kh1 Qb1 44.Kg2 Qb2 45.Kh1 Qa1 46.Kg2 Qa2 47.Kh1 Qfa8 48.Rxg7 Kxg7 49.Qe7 Kg8 50.Qe6 Kf8 51.Qxd6 Ke8 52.Qe6 Kd8 53.Qxb6 Kd7 54.Qb5 Qc6 55.Qf5 Kc7 56.Qf7 Qd7 0-1";
        //quickDebugView(g14);

        String g15 = " 1.c4 g6 2.Nc3 Bg7 3.g3 c5 4.Bg2 Nc6 5.Nf3 d6 6.O-O e6 7.d3 Nge7 8.Bg5 h6 9.Bd2 b6 10.Rb1 Bb7 11.a3 Qd7 12.b4 Rd8 13.e3 O-O 14.Qe2 Ba8 15.Rfd1 Qc8 16.Nb5 g5 17.h3 Ng6 18.Bc3 e5 19.Be1 f5 20.Nc3 Nce7 21.Nh2 Bxg2 22.Kxg2 Rf7 23.a4 Rdf8 24.a5 cxb4 25.Rxb4 Nc6 26.Rb5 Nxa5 27.Nd5 Nc6 28.Qh5 Nge7 29.Ra1 e4 30.d4 f4 31.Qg4 Rf5 32.Nxe7 Nxe7 33.Rxa7 fxe3 34.Rxe7 exf2 35.Rxf5 fxe1=Q 36.Rxf8 Qxf8 37.Qe6 Kh7 38.Rf7 Qd2 39.Kh1 Qe1 40.Kg2 Qe2 41.Kh1 Qd1 42.Kg2 Qc2 43.Kh1 Qb1 44.Kg2 Qb2 45.Kh1 Qa1 46.Kg2 Qa2 47.Kh1 Qfa8 48.Rxg7 Kxg7 49.Qe7 Kg8 50.Qe6 Kf8 51.Qxd6 Ke8 52.Qe6 Kd8 53.Qxb6 Kd7 54.Qb5 Qc6 55.Qf5 Kc7 56.Qf7 Qd7 0-1";
        String g15v = "        |  kq Q  |       p|      p |  PPp   |      PP|q      N|       K|";
        mg = new Game(g15);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g15v));

        String g16=" 1.Nf3 d5 2.c4 c6 3.e3 Nf6 4.Nc3 a6 5.h3 e6 6.b3 c5 7.cxd5 exd5 8.Bb2 b5 9.a4 b4 10.Ne2 Bd6 11.Rc1 Be6 12.Nf4 Bxf4 13.exf4 Qe7 14.Be2 d4 15.O-O O-O 16.Ne5 Ra7 17.Re1 Qd6 18.Qc2 Rc7 19.Bc4 Bxc4 20.Qxc4 Nd5 21.g3 Nb6 22.Qd3 Rfc8 23.Qf5 Qd5 24.Qd3 f6 25.a5 N6d7 26.Nc4 Rc6 27.Re7 Re6 28.Re1 Rxe1 29.Rxe1 Rd8 30.Re4 g6 31.Qe2 Rf8 32.d3 Nc6 33.Bc1 f5 34.Re6 Rf6 35.Re8 Nf8 36.Bd2 Re6 37.Rxe6 Qxe6 38.Qf3 Nd7 39.Kg2 Nf6 40.Ne5 Nd5 41.Nc4 Nc7 42.Kh2 Kf7 43.g4 Qd5 44.Qe2 Ne6 45.gxf5 gxf5 46.Qh5 Kg7 47.Ne5 Nxe5 48.fxe5 Qxe5 49.Kg1 Qf6 50.Qf3 Qf7 51.Qc6 f4 52.Qxa6 f3 53.Kf1 Qf5 54.Ke1 Qxh3 55.Kd1 Kf6 56.Qc6 Qf1 57.Kc2 Qxf2 58.a6 Qe2 59.a7 f2 60.a8=Q f1=Q 61.Qh8 Ke7 62.Qb7 Kd6 63.Qhb8 Nc7 64.Q8xc7 Ke6 65.Qbc8 Kf6 66.Q8d8 Kg6 67.Qg5 1-0";
        //quickDebugView(g16);
        String g16v="        |  Q    p|      k |  p   Q | p p    | P P    |  KBq   |     q  |";
        mg = new Game(g16);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g16v));

        String g17=" 1.c4 Nf6 2.g3 c5 3.Bg2 Nc6 4.Nf3 d5 5.cxd5 Nxd5 6.Nc3 Nc7 7.O-O e5 8.a3 Be7 9.b4 cxb4 10.axb4 e4 11.Nxe4 Nxb4 12.Bb2 O-O 13.d4 Ncd5 14.Ne5 f6 15.Nc4 Be6 16.Ned2 f5 17.Ne5 Bf6 18.e4 fxe4 19.Nxe4 Be7 20.h4 Rc8 21.Rxa7 Rc2 22.Bc1 Qb6 23.Ra3 Rfc8 24.Bg5 Bf8 25.Nc5 Bxc5 26.dxc5 Qxc5 27.Nd3 Qb6 28.Nxb4 Nxb4 29.Re3 Bf7 30.Re7 R2c7 31.Rfe1 Nc6 32.R7e4 Rf8 33.Bf4 Rcc8 34.Be3 Qc7 35.Bf4 Qb6 36.Be3 Qc7 37.Rf4 Rcd8 38.Qb1 Qd7 39.Bb6 Ra8 40.Rd1 Qe7 41.Qb5 Rfe8 42.Rxf7 Qxf7 43.Bd5 Re6 44.Qb3 Rae8 45.Kg2 Qf5 46.Re1 Kf7 47.Re3 Qg6 48.Qc4 h5 49.Qf4 Kg8 50.Qd6 Kf7 51.Rf3 Kg8 52.Re3 Kf7 53.Qd7 Re7 54.Qc8 Qg4 55.Bc5 Re8 56.Qxe8 Kxe8 57.Bxe6 1-0";
        String g17v="    k   | p    p |  n B   |  B    p|      qP|    R P |     PK |        |";
        mg = new Game(g17);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g17v));

        String g18=" 1.d4 Nf6 2.c4 e5 3.dxe5 Ng4 4.e4 Nxe5 5.Nc3 Bb4 6.Bd2 O-O 7.f4 Ng6 8.Qc2 Re8 9.Nge2 Nc6 10.O-O-O a5 11.h4 d6 12.h5 Nf8 13.h6 g6 14.Be1 Bg4 15.Bh4 f6 16.Nd5 g5 17.Bg3 Ng6 18.a3 Bc5 19.Qc3 Rf8 20.fxg5 Nce5 21.Nxf6 Rxf6 22.gxf6 Qxf6 23.Kb1 Qg5 24.Rc1 Be3 25.Rc2 a4 26.Be1 Ra6 27.Bd2 Bxd2 28.Rxd2 c5 29.Nc1 Rb6 30.Ka1 Nf8 31.Be2 Ne6 32.Bxg4 Qxg4 33.Re1 Qg5 34.Qh3 Nf8 35.Rf2 Neg6 36.Ref1 Qe7 37.Qg4 Kh8 38.Qc8 Kg8 39.Rf7 -- 40.Rh1 Qd8 41.Rc7 1-0";
        //quickDebugView(g18);

        String g19=" 1.d4 f5 2.g3 Nf6 3.Bg2 d6 4.Nf3 g6 5.O-O Bg7 6.c4 O-O 7.Nc3 Qe8 8.d5 a5 9.Nd4 Na6 10.e4 fxe4 11.Nxe4 Nxe4 12.Bxe4 Bh3 13.Re1 Nc5 14.Bh1 Qa4 15.b3 Qb4 16.Be3 a4 17.a3 Qc3 18.Rc1 Qb2 19.Rb1 Qxa3 20.Nb5 Qa2 21.Re2 axb3 22.Bxc5 Bg4 23.f3 Rxf3 24.Bd4 Rxg3 25.hxg3 Bxe2 26.Qc1 Rf8 27.Bg2 Qc2 28.Bf1 Be5 29.Ba1 Qd2 30.Bc3 b2 31.Ra1 bxc1=B 32.Nxc7 Bg7 33.Bf6 Qd3 34.Ra5 b5 35.Ne8 Bch6 36.Nc7 bxc4 37.Na6 Bd1 1-0";
        String g19v="     rk |    p bp|N  p Bpb|R  P    |  p     |   q  P |        |   b BK |";
        mg = new Game(g19);
        while(!mg.gameIsOver) mg.nextMove();
        System.out.println(mg.toString().equals(g19v));

        Tracker.printMatrix();
        Tracker.printSystems();
        //quickDebugView(g7);

        //ChessBoard view = new ChessBoard();
        //view.fromString(mg.toString());
        //view.printBoard();
    }
}
