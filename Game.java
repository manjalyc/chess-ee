public class Game {
    public ChessBoard board;
    private String history;
    private int currSpace;
    private int moveNumber;
    private boolean white;
    public static boolean debugOutput = false;
    public boolean gameIsOver;

    public Game(String history){
        this.history = " " + history
                .replaceAll("\\.\\ ", ".")
                .replaceAll("\\.\n", ".")
                .replaceAll("\n", " ")
                .replaceAll("  ", " ")
                .replaceAll("\\+", "")
                .replaceAll("#","")
                .trim()
        ;
        if(history.contains("--") || history.contains("...")) throw new IllegalArgumentException("Invalid Notation Detected.");
        if(debugOutput) System.out.println(this.history);
        board = new ChessBoard();
        currSpace = 0;
        white = true;
        gameIsOver = false;
        moveNumber = 0;
    }
    public String getHistory(){
        return this.history;
    }

    public void nextMove(){
        int nextSpace = history.indexOf(' ', currSpace+1);
        String move = "";
        if(nextSpace == -1){
            gameIsOver = true;
            Tracker.games++;
            return;
        }
        else move = history.substring(currSpace+1, nextSpace);
        if(white){
            int posOfPeriod = move.indexOf('.');
            move = move.substring(posOfPeriod + 1);
            moveNumber++;
        }
        if(debugOutput) System.out.println("move " + moveNumber +   ": " + move);
        if(move.charAt(0) == 'B')bishopMove(move);
        else if(move.charAt(0) == 'N')knightMove(move);
        else if(move.charAt(0) == 'R')rookMove(move);
        else if(move.charAt(0) == 'K')kingMove(move);
        else if(move.charAt(0) == 'Q')queenMove(move);
        else if(move.equals("O-O") || move.equals("O-O-O"))castle(move);
        else pawnMove(move);
        if(kingInDanger(white)) throw new IllegalStateException("King in Danger, Invalid Move #" + moveNumber + ": " + move + " | " + history);
        if(debugOutput) board.printBoard();
        currSpace = nextSpace;
        white = !white;
    }

    private boolean canPieceMoveIn1D(String source, String dest){
        int sc = ChessBoard.getColFromPos(source);
        int sr = ChessBoard.getRowFromPos(source);
        int dc = ChessBoard.getColFromPos(dest);
        int dr = ChessBoard.getRowFromPos(dest);
        if(sc == dc){
            for(int i = Math.min(sr,dr) + 1; i < Math.max(sr,dr); i++){
                if(board.getPiece(i,sc) != ' ')return false;
            }
            return true;
        }
        if(sr == dr){
            for(int j = Math.min(sc,dc) + 1; j < Math.max(sc,dc); j++){
                if(board.getPiece(sr,j) != ' ')return false;
            }
            return true;
        }
        return false;
    }
    public boolean canPieceMoveDiagonallyTo(String source, String dest){
        int sc = ChessBoard.getColFromPos(source);
        int sr = ChessBoard.getRowFromPos(source);
        int dc = ChessBoard.getColFromPos(dest);
        int dr = ChessBoard.getRowFromPos(dest);
        if(Math.abs(sc - dc) != Math.abs(sr - dr)) return false;
        while(sc < dc - 1 && sr < dr - 1) if(board.getPiece(++sr,++sc) != ' ')return false;
        while(sc > dc + 1 && sr < dr - 1) if(board.getPiece(++sr,--sc) != ' ')return false;
        while(sc < dc - 1 && sr > dr + 1) if(board.getPiece(--sr,++sc) != ' ')return false;
        while(sc > dc + 1 && sr > dr + 1){
            if(board.getPiece(--sr,--sc) != ' ')return false;
        }
        return true;
    }
    private boolean willItBeInCheck(String guardPos, String destPos, boolean white){
        boolean inCheck = false;
        char guardPiece = board.getPiece(guardPos);
        char destPiece = board.getPiece(destPos);
        board.setPiece(destPos, guardPiece);
        board.setPiece(guardPos, ' '); //temporary
        if(kingInDanger(white)) {
            board.setPiece(guardPos, guardPiece);
            board.setPiece(destPos, destPiece);
            return true;
        }
        board.setPiece(guardPos, guardPiece);
        board.setPiece(destPos, destPiece);
        return false;
    }
    public boolean kingInDanger(boolean white){
        String kingPos = board.findSourceKing(white);
        if(white){
            if(board.diagonalScan(kingPos, 'b') != null) return true;
            if(board.rowScan(kingPos, 'r') != null) return true;
            if(board.colScan(kingPos, 'r') != null) return true;
            if(board.rowScan(kingPos, 'q') != null) return true;
            if(board.colScan(kingPos, 'q') != null) return true;
            if(board.diagonalScan(kingPos, 'q') != null) return true;
            int kr = ChessBoard.getRowFromPos(kingPos);
            int kc = ChessBoard.getColFromPos(kingPos);
            if(kc - 1 >= 0 && kr - 1 >= 0 && board.getPiece(ChessBoard.toPos(kr-1, kc-1)) == 'p') return true;
            if(kc + 1 < 8 && kr - 1 >= 0 && board.getPiece(ChessBoard.toPos(kr-1,kc+1)) == 'p') return true;
        }
        else{
            //System.out.println(1);
            if(board.diagonalScan(kingPos, 'B') != null) return true;
            //System.out.println(1);
            if(board.rowScan(kingPos, 'R') != null) return true;
            if(board.colScan(kingPos, 'R') != null) return true;
            //System.out.println(1);
            if(board.rowScan(kingPos, 'Q') != null) return true;
            if(board.colScan(kingPos, 'Q') != null) return true;
            if(board.diagonalScan(kingPos, 'Q') != null) return true;
            int kr = ChessBoard.getRowFromPos(kingPos);
            int kc = ChessBoard.getColFromPos(kingPos);
            if(kc - 1 >= 0 && kr + 1 < 8 && board.getPiece(ChessBoard.toPos(kr+1, kc-1)) == 'P') return true;
            if(kc + 1 < 8 && kr + 1 < 8 && board.getPiece(ChessBoard.toPos(kr+1, kc+1)) == 'P') return true;
        }

        return false;
    }

    private void pawnMove(String move){
        String dest = move.substring(move.length()-2);
        if(move.length() == 2){
            String sourcePos = board.findSourcePawnInCol(dest, white);
            board.movePiece(sourcePos, dest);
            return;
        }
        if(move.length() == 4){
            if(move.contains("=")){
                dest = move.substring(0,2);
                String sourcePos = board.findSourcePawnInCol(dest, white);
                board.movePiece(sourcePos, dest);
                if(white) board.setPiece(dest, Character.toUpperCase(move.charAt(3)) );
                else board.setPiece(dest, Character.toLowerCase(move.charAt(3)) );
                return;
            }
            else if(board.getPiece(dest) != ' '){
                String sourcePos = board.findSourcePawnInCross(dest, move.charAt(0), white);
                board.movePiece(sourcePos, dest);
                return;
            }
            //for en passant
            else{
                String sourcePos = board.findSourcePawnInCross(dest, move.charAt(0), white);
                //remove the en passant captured piece
                int destRow = ChessBoard.getRowFromPos(dest);
                int destCol = ChessBoard.getColFromPos(dest);
                if(white) board.removePiece(ChessBoard.toPos(destRow+1,destCol));
                else board.removePiece(ChessBoard.toPos(destRow-1,destCol));
                //move the pawn
                board.movePiece(sourcePos, dest);
                return;
            }
        }
        if(move.length() == 6 && move.contains("x") && move.contains("=")){
            dest = move.substring(2,4);
            String sourcePos = board.findSourcePawnInCross(dest, move.charAt(0), white);
            board.movePiece(sourcePos, dest);
            if(white) board.setPiece(dest, Character.toUpperCase(move.charAt(5)) );
            else board.setPiece(dest, Character.toLowerCase(move.charAt(5)) );
            return;
        }
        throw new IllegalStateException("Not Ready for move: " + move + " | " + history);
    }
    private void bishopMove(String move){
        move = move.replace("x","");
        String dest = move.substring(move.length()-2);
        if( move.length() == 3) {
            String sourcePos = board.findSourceBishop(dest, white);
            board.movePiece(sourcePos, dest);
            return;
        }
        if( move .length() == 4){
            if(white) for(String bishopPos : board.getAllPositionsOf('B')){
                if( (bishopPos.charAt(0) == move.charAt(1) || bishopPos.charAt(1) == move.charAt(1)) && canPieceMoveDiagonallyTo(bishopPos, dest) && !willItBeInCheck(bishopPos, dest, white) ) {
                    board.movePiece(bishopPos, dest);
                    return;
                }
            }
            else for(String bishopPos : board.getAllPositionsOf('b')){
                if( (bishopPos.charAt(0) == move.charAt(1) || bishopPos.charAt(1) == move.charAt(1)) && canPieceMoveDiagonallyTo(bishopPos, dest) && !willItBeInCheck(bishopPos, dest, white) ) {
                    board.movePiece(bishopPos, dest);
                    return;
                }
            }
        }
        throw new IllegalStateException("Not Ready for move: " + move + " | " + history);
    }
    private void knightMove(String move){
        move = move.replace("x","");
        String dest = move.substring(move.length()-2);
        if( move.length() == 3 || (move.length() == 4 && move.charAt(1) == 'x')) {
            String k1 = board.findSourceKnight(dest, white);
            String k2 = board.findSourceKnightAlt(dest, white);
            if(!k1.equals(k2)) {
                if (!willItBeInCheck(k1, dest, white) && willItBeInCheck(k2, dest, white)) {
                    String sourcePos = k1;
                    board.movePiece(sourcePos, dest);
                    return;
                } else if (!willItBeInCheck(k2, dest, white) && willItBeInCheck(k1, dest, white)) {
                    String sourcePos = k2;
                    board.movePiece(sourcePos, dest);
                    return;
                }
                else throw new IllegalStateException("Ambiguous Knight: " + "Move #" + moveNumber + ", Dest: " + dest + ", White?=" + white);
            }
            String sourcePos = k1;
            board.movePiece(sourcePos, dest);
            return;
        }
        if(move.length() == 4) {
            String sourcePos = null;
            if(white) {
                for(String knightPos : board.getAllPositionsOf('N')){
                    if(knightPos.charAt(0) == move.charAt(1)) sourcePos = knightPos;
                    else if(knightPos.charAt(1) == move.charAt(1)) sourcePos = knightPos;
                }
            }
            else{
                for(String knightPos : board.getAllPositionsOf('n')){
                    if(knightPos.charAt(0) == move.charAt(1)) sourcePos = knightPos;
                    else if(knightPos.charAt(1) == move.charAt(1)) sourcePos = knightPos;
                }
            }
            board.movePiece(sourcePos, dest);
            return;
        }
        throw new IllegalStateException("Not Ready for move: " + move + " | " + history);
    }
    private void rookMove(String move){
        move = move.replace("x","");
        String dest = move.substring(move.length()-2);
        if( move.length() == 3) {
            if(white) {
                for (String rookPos : board.getAllPositionsOf('R')){
                    if(rookCanMoveTo(rookPos, dest) && !willItBeInCheck(rookPos, dest, white)){
                        String sourcePos = board.findSourceRook(dest, white);
                        board.movePiece(rookPos, dest);
                        return;
                    }
                }
            }
            else {
                for (String rookPos : board.getAllPositionsOf('r')){
                    if(rookCanMoveTo(rookPos, dest) && !willItBeInCheck(rookPos, dest, white)){
                        board.movePiece(rookPos, dest);
                        return;
                    }
                }
            }
        }
        if(move.length() == 4) {
            String sourcePos = null;
            if(white) {
                for(String rookPos : board.getAllPositionsOf('R')){
                    if(rookPos.charAt(0) == move.charAt(1)) sourcePos = rookPos;
                    else if(rookPos.charAt(1) == move.charAt(1)) sourcePos = rookPos;
                }
            }
            else{
                for(String rookPos : board.getAllPositionsOf('r')) {
                    if (rookPos.charAt(0) == move.charAt(1)) sourcePos = rookPos;
                    else if(rookPos.charAt(1) == move.charAt(1)) sourcePos = rookPos;
                }
            }
            board.movePiece(sourcePos, dest);
            return;
        }
        throw new IllegalStateException("Not Ready for move: " + move +  " | " + history);
    }
    private boolean rookCanMoveTo(String source, String dest){
        return canPieceMoveIn1D(source, dest);
    }
    private boolean queenCanMoveTo(String source, String dest){
        return canPieceMoveIn1D(source, dest) || canPieceMoveDiagonallyTo(source, dest);
    }
    private void queenMove(String move){
        move = move.replace("x","");
        String dest = move.substring(move.length() - 2);
        if(move.length() == 3) {
            if(white) {
                for(String queenPos : board.getAllPositionsOf('Q')){
                    if( queenCanMoveTo(queenPos, dest) && !willItBeInCheck(queenPos, dest, white)){
                        board.movePiece(queenPos, dest);
                        return;
                    }
                }
            }
            else {
                for(String queenPos : board.getAllPositionsOf('q')){
                    if( queenCanMoveTo(queenPos, dest) && !willItBeInCheck(queenPos, dest, white) ){
                        board.movePiece(queenPos, dest);
                        return;
                    }
                }
            }
        }
        else if(move.length() == 4){
            if(white) {
                for(String queenPos : board.getAllPositionsOf('Q')){
                    if( (queenPos.charAt(0) == move.charAt(1) || queenPos.charAt(1) == move.charAt(1)) && queenCanMoveTo(queenPos, dest) && !willItBeInCheck(queenPos, dest, white) ){
                        board.movePiece(queenPos, dest);
                        return;
                    }
                }
            }
            else {
                for(String queenPos : board.getAllPositionsOf('q')){
                    if( (queenPos.charAt(0) == move.charAt(1) || queenPos.charAt(1) == move.charAt(1)) && queenCanMoveTo(queenPos, dest) && !willItBeInCheck(queenPos, dest, white) ){
                        board.movePiece(queenPos, dest);
                        return;
                    }
                }
            }
        }
        System.out.println(history);
        throw new IllegalStateException("Not Ready for move: " + move + " | " + history);
    }
    private void kingMove(String move){
        move = move.replace("x","");
        String dest = move.substring(move.length()-2);
        String sourcePos = board.findSourceKing(white);
        board.movePiece(sourcePos, dest);
        return;
    }
    private void castle(String move){
        if(white && move.equals("O-O")){
            board.movePiece("e1","g1");
            board.movePiece("h1","f1");
        }
        else if(!white && move.equals("O-O")){
            board.movePiece("e8","g8");
            board.movePiece("h8","f8");

        }
        else if(white && move.equals("O-O-O")){
            board.movePiece("e1","c1");
            board.movePiece("a1","d1");
        }
        else if(!white && move.equals("O-O-O")){
            board.movePiece("e8","c8");
            board.movePiece("a8","d8");
        }
    }

    public String toString() {
        return board.toString();
    }
    public void printBoard() {board.printBoard();}
}
