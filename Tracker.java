public class Tracker {
    public static int games = 0;
    public static int[][] taken = {
            {0,0,0,0,0,0},
            {0,0,0,0,0,0},
            {0,0,0,0,0,0},
            {0,0,0,0,0,0},
            {0,0,0,0,0,0},
            {0,0,0,0,0,0},
    };

    private static char intToPiece(int value){
        if( value == 0 ) return 'p';
        if( value == 1 ) return 'b';
        if( value == 2 ) return 'n';
        if( value == 3 ) return 'r';
        if( value == 4 ) return 'q';
        if( value == 5 ) return 'k';
        throw new IllegalArgumentException("Invalid Piece Value: " + value);
    }

    private static int pieceToInt(char piece){
        piece = Character.toLowerCase(piece);
        if( piece == 'p' ) return 0;
        if( piece == 'b' ) return 1;
        if( piece == 'n' ) return 2;
        if( piece == 'r' ) return 3;
        if( piece == 'q' ) return 4;
        if( piece == 'k' ) return 5;
        throw new IllegalArgumentException("Invalid Piece: " + piece);
    }

    public static void logTaken(char newpiece, char lostpiece){
        if( lostpiece == ' ' ) return;
        taken[pieceToInt(newpiece)][pieceToInt(lostpiece)]++;
    }

    public static void printMatrix(){
        System.out.println("Matrix for " + games + " games.");
        for(int i = 0; i < 6; i++){
            System.out.print(intToPiece(i) + ": " );
            for(int j = 0; j < 6; j++){
                System.out.print(taken[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void printSystems(){
        System.out.print(games*16 + "p = " );
        for(int j = 0; j < 6; j++){
            System.out.print(taken[0][j] + Character.toString(intToPiece(j)) + " + ");
        }
        System.out.println();
        System.out.print(games*4 + "b = " );
        for(int j = 0; j < 6; j++){
            System.out.print(taken[1][j] + Character.toString(intToPiece(j)) + " + ");
        }
        System.out.println();
        System.out.print(games*4 + "n = " );
        for(int j = 0; j < 6; j++){
            System.out.print(taken[2][j] + Character.toString(intToPiece(j)) + " + ");
        }
        System.out.println();
        System.out.print(games*4 + "r = " );
        for(int j = 0; j < 6; j++){
            System.out.print(taken[3][j] + Character.toString(intToPiece(j)) + " + ");
        }
        System.out.println();
        System.out.print(games*2 + "q = " );
        for(int j = 0; j < 6; j++){
            System.out.print(taken[4][j] + Character.toString(intToPiece(j)) + " + ");
        }
        System.out.println();
        System.out.print(games*2 + "k = " );
        for(int j = 0; j < 6; j++){
            System.out.print(taken[5][j]  + Character.toString(intToPiece(j)) + " + ");
        }
        System.out.println();
    }
}
